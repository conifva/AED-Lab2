#include <iostream>
#include <stdio.h>
#include "Pila.h"

using namespace std;

void menu(Pila pila){
	int opcion;
	char elemento;
	
	cout << "\n\t\tMENU\n" << endl;
	cout << " [1] Agregar elemento / Push" << endl;
	cout << " [2] Remover elemento / Pop" << endl;
	cout << " [3] Ver pila / Show" << endl;
	cout << " [4] Salir / Exit" << endl;
	
	cout << "\n Ingrese su opción: ";
	cin >> opcion;

	while(getchar() != '\n');

	switch(opcion){
		case 1:
				cout << "\n\tAGREGAR ELEMENTO PILA\n" << endl;
			cout << "Ingrese un elemento: ";
			cin >> elemento;
			pila.Push(elemento);
			menu(pila);
			break;

		case 2:
			cout << "Se eliminará el ultimo elemento ingresado," << endl;
			cout << "el que esta en la parte superior de la pila" << endl;
			cout << "\nPresione una tecla para continuar" << endl;
			cin >> elemento;
			pila.Pop();
			menu(pila);
			break;

		case 3:
			pila.Show();
			menu(pila);
			break;

		case 4:
			exit(1);
			break;

		default:
			cout << "Opcion no valida" << endl;
			break;
	}

}

int main(){
	int limite; //capacidad de la pila

	cout << "\n\t\tBIENVENIDO\n" << endl;
	cout << "Ingrese el tamaño de su pila: " << endl;
	cin >> limite;

	Pila pila = Pila(limite);
	menu(pila);
	
	return 0;
}

# AED-Lab2
Actividades correspondientes al Laboratorio 1 de Algortimo y Estructura de Datos.

# Lab2-UI
Programa para manipular y mostrar una pila.

# Obtención del Programa
Clonar repositorio, ingresar a la carpeta clonada y ejecutar por terminal:
```
g++ programa.cpp Pila.cpp -o programa
make
./programa
```

# Acerca de
El programa solicita inicialmente que el usuario ingrese el tamaño máximo de la pila, luego se presentará un menú con 4 opciones. 

La primera consiste en agregar elementos a la pila recien creada, en caso de seleccionarla se pedira que ingrese UN caracter o elemento. La segunda opción permite eliminar elementos anteriormente ingresados, solo se permite eliminar UN elemento a la vez y este será el ultimo que ingresó el usuario. En opción tres se encarga de mostrar el contenido de la pila. Finalmente la cuarta opción le permite al usuario cerrar el programa.

# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)

# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- Editor utilizado: Sublime Text

# Autor
Constanza Valenzuela

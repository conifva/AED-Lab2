#include <iostream>
#include "Pila.h"

using namespace std;

/* constructor */
Pila::Pila(int limite){
	this->limite = limite;
	this->arreglo = new char [this->limite];
}

/* NOTA puede que este de mas esta */
int Pila::get_limite(){
	return this->limite;
}

void Pila::set_limite(int limite){
	this->limite = limite;
}

/* boolean para ver estado de la pila */
void Pila::Pila_vacia(){
	this->tope = tope;
	if(tope == 0){
		this->band = true; // pila vacia
	}
	else{
		this->band = false; // pila llena 
	}
}

/* boolean para ver estado de la pila */
void Pila::Pila_llena(){
	this->tope = tope;
	if(tope == this->limite){
		this->band = true; //pila llena
	}
	else{
		this->band = false; //pila con espacio
	}
	
}

/* agregar elemntos*/
void Pila::Push(char elemento){
	
	/* ver estado*/
	Pila_llena();
    this->band = band;
    this->tope = tope;

	if (band == true){
		cout << "[Desbordamiento]" << endl;
		cout << "Pila llena, elimine algun/os elemento/s para poder agregar" << endl;
	}
	else{
		tope = (tope + 1);
		// Se añade el elemento en el nuevo tope
		this->arreglo[tope] = elemento;
		cout << "Elemento ingresado con exito" << endl;
	}
}

/* sacar elementos */
void Pila::Pop(){
	cout << "\n\tELIMINAR ELEMENTO PILA\n" << endl;
	
	/* comprobar estado */
	Pila_vacia();
	this->band = band;
    this->tope = tope;

	/*caso vacia */
	if (band == true){
		cout << "[Subdesbordamiento]" << endl;
		cout << "Pila vacı́a, ingrese algun/os elemento/s para poder eliminar" << endl;
	}
	/* caso con elementos (me da lo mismo si esta llena) */
	else{
		elemento = this->arreglo[tope];
		tope = (tope - 1);
		cout << "El elemento eliminado es: " << elemento << endl;
	}
}

/* mostrar contenido pila */
void Pila::Show(){
	cout << "\n\tMOSTRAR PILA\n" << endl;
	
	this->tope=tope;
	for(int i = tope; i >= 1; i--){
			cout << "\t|" << arreglo[i] << "|" << endl;
	}
	/*
	//ARREGAAAAAR
	//caso vacia o llena
	if (this->band == true){
		cout << "Pila vacı́a, ingrese elementos para poder visualizar" << endl;
		/* TODO
		 * DIBUJAR UNA BONITA PILA VACIA
		 *
	
	}
	// caso con elementos
	else{
		// último elemento agregado es el primero en imprir 
		for(int i = tope; i >= 1; i--){
			cout << "\t|" << arreglo[i] << "|" << endl;
		}
	}
	*/	
}

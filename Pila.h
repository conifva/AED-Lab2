#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila{
	private:
		int tope = 0;
		int limite;
		char *arreglo = NULL;
		char elemento;
		bool band;

	public:
		/* constructores */ 
		Pila(int limite);

		/* métodos get and set */
		int get_limite();

		void set_limite(int limite);
		void Pila_vacia();
		void Pila_llena();

		/* opciones menu */
		void Push(char elemento);
		void Pop();
		void Show();

};
#endif
